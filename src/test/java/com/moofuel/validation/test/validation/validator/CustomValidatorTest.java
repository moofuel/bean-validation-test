package com.moofuel.validation.test.validation.validator;

import com.moofuel.validation.test.domain.TestKek;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.beanvalidation.SpringValidatorAdapter;

import javax.validation.Validation;
import javax.validation.Validator;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Дмитрий
 * @since 17.10.2017
 */
@RunWith(Theories.class)
public class CustomValidatorTest {

    private SpringValidatorAdapter adapter;
    private CustomValidator customValidator;

    @DataPoints
    public static TestKek[] testKeks() {
        return TestKek.values();
    }

    @Before
    public void setup() {
        final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
        this.adapter = new SpringValidatorAdapter(validator);
        this.customValidator = new CustomValidator(adapter);
    }

    @Test
    @Theory
    public void kek(TestKek testKek) throws Exception {
        final com.moofuel.validation.test.domain.Test test = new com.moofuel.validation.test.domain.Test();
        test.setCheburek(testKek);
        final BeanPropertyBindingResult bindingResult = new BeanPropertyBindingResult(test, "test");
        adapter.validate(test, bindingResult);
        customValidator.validate(test, bindingResult);
        assertThat(bindingResult.getAllErrors()).size().isEqualTo(2);
    }

    @Test
    @Theory
    public void lol(TestKek testKek) throws Exception {
        final com.moofuel.validation.test.domain.Test test = new com.moofuel.validation.test.domain.Test();
        test.setCheburek(testKek);
        test.setWat("wat");
        final BeanPropertyBindingResult bindingResult = new BeanPropertyBindingResult(test, "test");
        adapter.validate(test, bindingResult);
        customValidator.validate(test, bindingResult);
        assertThat(bindingResult.getAllErrors()).size().isEqualTo(1);
    }

    @Test
    @Theory
    public void wat(TestKek testKek) throws Exception {
        final com.moofuel.validation.test.domain.Test test = new com.moofuel.validation.test.domain.Test();
        test.setCheburek(testKek);
        test.setWat("wat");
        test.setKek("kek");
        final BeanPropertyBindingResult bindingResult = new BeanPropertyBindingResult(test, "test");
        adapter.validate(test, bindingResult);
        customValidator.validate(test, bindingResult);
        switch (testKek) {
            case KEK:
                assertThat(bindingResult.getAllErrors()).size().isEqualTo(0);
                break;
            case LOL:
                assertThat(bindingResult.getAllErrors()).size().isEqualTo(1);
                break;
        }
    }

    @Test
    @Theory
    public void cheburek(TestKek testKek) throws Exception {
        final com.moofuel.validation.test.domain.Test test = new com.moofuel.validation.test.domain.Test();
        test.setCheburek(testKek);
        test.setWat("wat");
        test.setLol("lol");
        final BeanPropertyBindingResult bindingResult = new BeanPropertyBindingResult(test, "test");
        adapter.validate(test, bindingResult);
        customValidator.validate(test, bindingResult);
        switch (testKek) {
            case KEK:
                assertThat(bindingResult.getAllErrors()).size().isEqualTo(1);
                break;
            case LOL:
                assertThat(bindingResult.getAllErrors()).size().isEqualTo(0);
                break;
        }
    }

}