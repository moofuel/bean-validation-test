package com.moofuel.validation.test.domain;

import com.moofuel.validation.test.validation.marker.Kek;
import com.moofuel.validation.test.validation.marker.Lol;

import javax.validation.constraints.NotNull;

/**
 * @author Дмитрий
 * @since 16.10.2017
 */
public class Test {

    @NotNull(groups = Kek.class, message = "KEKKKKKKKKER")
    private String kek;
    @NotNull(groups = Lol.class, message = "LOLLLLLLLLER")
    private String lol;
    @NotNull
    private TestKek cheburek;
    @NotNull
    private String wat;

    public String getKek() {
        return kek;
    }

    public void setKek(String kek) {
        this.kek = kek;
    }

    public String getLol() {
        return lol;
    }

    public void setLol(String lol) {
        this.lol = lol;
    }

    public TestKek getCheburek() {
        return cheburek;
    }

    public void setCheburek(TestKek cheburek) {
        this.cheburek = cheburek;
    }

    public String getWat() {
        return wat;
    }

    public void setWat(String wat) {
        this.wat = wat;
    }

    @Override
    public String toString() {
        return "Test{" +
                "kek='" + kek + '\'' +
                ", lol='" + lol + '\'' +
                ", cheburek=" + cheburek +
                ", wat='" + wat + '\'' +
                '}';
    }
}
