package com.moofuel.validation.test.domain;

/**
 * @author Дмитрий
 * @since 16.10.2017
 */
public enum TestKek {
    LOL, KEK
}
