package com.moofuel.validation.test.validation.validator;

import com.moofuel.validation.test.domain.Test;
import com.moofuel.validation.test.domain.TestKek;
import com.moofuel.validation.test.validation.marker.Kek;
import com.moofuel.validation.test.validation.marker.Lol;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.SmartValidator;
import org.springframework.validation.Validator;

/**
 * @author Дмитрий
 * @since 16.10.2017
 */
@Component
public class CustomValidator implements Validator {

    private SmartValidator validator;

    public CustomValidator(SmartValidator validator) {
        this.validator = validator;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return Test.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        final Test test = (Test) target;
        final TestKek cheburek = test.getCheburek();
        if (cheburek == null) {
            return;
        }
        switch (cheburek) {
            case LOL:
                this.validator.validate(test, errors, Lol.class);
                break;
            case KEK:
                this.validator.validate(test, errors, Kek.class);
                break;
            default:
                throw new IllegalStateException("Some fuckery flows in this code");
        }
    }
}
