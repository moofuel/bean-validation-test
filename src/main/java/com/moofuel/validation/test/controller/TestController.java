package com.moofuel.validation.test.controller;

import com.moofuel.validation.test.domain.Test;
import com.moofuel.validation.test.validation.validator.CustomValidator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

/**
 * @author Дмитрий
 * @since 16.10.2017
 */
@RestController
@RequestMapping("/test")
public class TestController {

    private CustomValidator customValidator;

    public TestController(CustomValidator customValidator) {
        this.customValidator = customValidator;
    }

    @InitBinder("test")
    public void setupBinder(WebDataBinder binder) {
        binder.addValidators(customValidator);
    }

    @PostMapping
    public Test kek(@Validated @RequestBody Test test) {
        return test;
    }
}
